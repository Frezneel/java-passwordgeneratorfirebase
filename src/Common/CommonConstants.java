package Common;

import java.awt.*;

public class CommonConstants {
    // color hex values
    public static final Color PRIMARY_COLOR = Color.decode("#b6fdc5");
    public static final Color SECONDARY_COLOR = Color.decode("#7fffd4");
    public static final Color TEXT_COLOR = Color.decode("#20b2aa");
    public static final Color RARE_BUTTON = Color.decode("#00ced1");

    public static final Color RED_COLOR = Color.decode("#ff7f7f");
    public static final Color WHITE_COLOR = Color.decode("#e4e4e4");
}
