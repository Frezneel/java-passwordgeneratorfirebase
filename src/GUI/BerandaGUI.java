package GUI;

import Common.CommonConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BerandaGUI extends FormGUI{
    public BerandaGUI() {
        super("Beranda");
        buildBeranda();
    }

    //Front-End
    private void buildBeranda(){
        JLabel berandaLabel = new JLabel("Beranda");
        berandaLabel.setBounds(0,25,520,100);
        berandaLabel.setForeground(CommonConstants.TEXT_COLOR);
        berandaLabel.setFont(new Font("Popins", Font.BOLD, 38));
        berandaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(berandaLabel);

        JButton generatorBtn = new JButton("Generate");
        generatorBtn.setBounds(145, 155, 220, 50);
        generatorBtn.setFont(new Font("Popins", Font.BOLD, 16));
        generatorBtn.setVerticalAlignment(JButton.CENTER);
        generatorBtn.setForeground(CommonConstants.TEXT_COLOR);
        generatorBtn.setBackground(CommonConstants.SECONDARY_COLOR);
        generatorBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        add(generatorBtn);
        generatorBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BerandaGUI.this.dispose();
                new PasswordGUI().setVisible(true);
            }
        });

        JButton myGeneratorBtn = new JButton("Password");
        myGeneratorBtn.setBounds(145, 235, 220, 50);
        myGeneratorBtn.setFont(new Font("Popins", Font.BOLD, 16));
        myGeneratorBtn.setVerticalAlignment(JButton.CENTER);
        myGeneratorBtn.setForeground(CommonConstants.TEXT_COLOR);
        myGeneratorBtn.setBackground(CommonConstants.SECONDARY_COLOR);
        myGeneratorBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        add(myGeneratorBtn);

        JButton logoutBtn = new JButton("Keluar Akun");
        logoutBtn.setBounds(145, 315, 220, 50);
        logoutBtn.setFont(new Font("Popins", Font.BOLD, 16));
        logoutBtn.setVerticalAlignment(JButton.CENTER);
        logoutBtn.setForeground(CommonConstants.WHITE_COLOR);
        logoutBtn.setBackground(CommonConstants.RED_COLOR);
        logoutBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        add(logoutBtn);

        JLabel aboutMe = new JLabel("Dibuat Oleh : Galih Muhammad Ichsan");
        aboutMe.setBounds(0, 550,520,100);
        aboutMe.setForeground(CommonConstants.TEXT_COLOR);
        aboutMe.setFont(new Font("Popins", Font.BOLD, 12));
        aboutMe.setHorizontalAlignment(SwingConstants.CENTER);
        add(aboutMe);

    }
}
