package GUI;


import Common.CommonConstants;
import LOGIC.PasswordGenerate;
import com.google.firebase.database.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class PasswordGUI extends FormGUI {

    public PasswordGUI(){
        super("Password Generator");
        passwordGenerate = new PasswordGenerate();
        passwordGUIBuild();
        // setDatabase();
    }

    private PasswordGenerate passwordGenerate;

    private void passwordGUIBuild(){
        JLabel pgLabel = new JLabel("Password Generator");
        pgLabel.setBounds(0,25,520,60);
        pgLabel.setForeground(CommonConstants.TEXT_COLOR);
        pgLabel.setFont(new Font("Popins", Font.BOLD, 38));
        pgLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(pgLabel);

        JTextField passwordField = new JTextField("");
        passwordField.setBounds(25,125,460,60);
        passwordField.setForeground(CommonConstants.TEXT_COLOR);
        passwordField.setHorizontalAlignment(JTextField.CENTER);
        passwordField.setEditable(false);
        passwordField.setFont(new Font("Popins", Font.BOLD, 18));
        add(passwordField);

        JLabel pgPanjang = new JLabel("Panjang Password: ");
        pgPanjang.setBounds(25,200,520,60);
        pgPanjang.setForeground(CommonConstants.TEXT_COLOR);
        pgPanjang.setFont(new Font("Popins", Font.BOLD, 16));
        add(pgPanjang);

        JTextField LPassword = new JTextField("6");
        LPassword.setBounds(200,200,60,60);
        LPassword.setForeground(CommonConstants.TEXT_COLOR);
        LPassword.setFont(new Font("Popins", Font.BOLD, 18));
        LPassword.setHorizontalAlignment(JTextField.CENTER);
        add(LPassword);

        JToggleButton bUppercase = new JToggleButton("Uppercase");
        bUppercase.setBounds(25,275,200,60);
        bUppercase.setForeground(CommonConstants.TEXT_COLOR);
        bUppercase.setFont(new Font("Popins", Font.BOLD, 18));
        bUppercase.setHorizontalAlignment(JTextField.CENTER);
        add(bUppercase);

        JToggleButton bLowercase = new JToggleButton("Lowercase");
        bLowercase.setBounds(275,275,200,60);
        bLowercase.setForeground(CommonConstants.TEXT_COLOR);
        bLowercase.setFont(new Font("Popins", Font.BOLD, 18));
        bLowercase.setHorizontalAlignment(JTextField.CENTER);
        add(bLowercase);

        JToggleButton bNumbers = new JToggleButton("Numbers");
        bNumbers.setBounds(25,350,200,60);
        bNumbers.setForeground(CommonConstants.TEXT_COLOR);
        bNumbers.setFont(new Font("Popins", Font.BOLD, 18));
        bNumbers.setHorizontalAlignment(JTextField.CENTER);
        add(bNumbers);

        JToggleButton bSymbols = new JToggleButton("Symbols");
        bSymbols.setBounds(275,350,200,60);
        bSymbols.setForeground(CommonConstants.TEXT_COLOR);
        bSymbols.setFont(new Font("Popins", Font.BOLD, 18));
        bSymbols.setHorizontalAlignment(JTextField.CENTER);
        add(bSymbols);

        JButton bGenerate = new JButton("GENERATE");
        bGenerate.setBounds(25,425,200,60);
        bGenerate.setForeground(CommonConstants.TEXT_COLOR);
        bGenerate.setFont(new Font("Popins", Font.BOLD, 18));
        bGenerate.setHorizontalAlignment(JTextField.CENTER);
        bGenerate.setBackground(CommonConstants.RARE_BUTTON);
        bGenerate.setForeground(CommonConstants.WHITE_COLOR);
        add(bGenerate);
        bGenerate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int minimal = 5;
                boolean anyToggleSelected = bUppercase.isSelected() || bLowercase.isSelected()
                        ||bNumbers.isSelected() || bSymbols.isSelected();
                if (LPassword.getText().length() <=0) return;
                if(Integer.parseInt(LPassword.getText()) <= minimal){JOptionPane.showMessageDialog(PasswordGUI.this,"Panjang Password Minimal 6" );}
                else{
                    int panjangPassword =Integer.parseInt(LPassword.getText().toString());

                    if(anyToggleSelected){
                        String generatedPassword = passwordGenerate.generatePassword(panjangPassword,
                                bUppercase.isSelected(), bLowercase.isSelected(),
                                bNumbers.isSelected(), bSymbols.isSelected()
                        );
                        passwordField.setText(generatedPassword);
                    }else{
                        JOptionPane.showMessageDialog(PasswordGUI.this,"Pilih Menu Minimal 1" );
                    }
                }
            }
        });

        JButton bSave = new JButton("SAVE");
        bSave.setBounds(275,425,200,60);
        bSave.setForeground(CommonConstants.TEXT_COLOR);
        bSave.setFont(new Font("Popins", Font.BOLD, 18));
        bSave.setHorizontalAlignment(JTextField.CENTER);
        bSave.setBackground(CommonConstants.RED_COLOR);
        bSave.setForeground(CommonConstants.WHITE_COLOR);
        add(bSave);

        JButton bKembali = new JButton("HOME");
        bKembali.setBounds(150,500,200,60);
        bKembali.setForeground(CommonConstants.TEXT_COLOR);
        bKembali.setFont(new Font("Popins", Font.BOLD, 18));
        bKembali.setHorizontalAlignment(JTextField.CENTER);
        bKembali.setBackground(CommonConstants.SECONDARY_COLOR);
        bKembali.setForeground(CommonConstants.TEXT_COLOR);
        add(bKembali);
        bKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PasswordGUI.this.dispose();
                BerandaGUI berandaGUI = new BerandaGUI();
                berandaGUI.setVisible(true);
            }
        });

    }

    private void setDatabase(){
        // Dapatkan referensi ke Firebase Database
        /*String wewe = "wewe";

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        database.child("tes").child("tos").setValueAsync("wee");
        database.child("tes").child("tos").child("tomlol").setValue(wewe, null);
        database.child("tes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String tombol = snapshot.child("nilai").getValue().toString();
                System.out.println(tombol);
                // Lakukan operasi database, seperti menulis dan membaca data

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

         */



    }
}
