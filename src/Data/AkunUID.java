package Data;

public class AkunUID {
    String UID;
    String Username;
    String Email;
    String Password;
    String NomorHP;

    public AkunUID(){}

    public AkunUID(String UID, String Username, String password){
        this.UID = UID;
        this.Username = Username;
        this.Password = password;
    }
    public AkunUID(String UID, String Username, String email, String password){
        this.UID = UID;
        this.Username = Username;
        this.Email = email;
        this.Password = password;
    }
    public AkunUID(String UID, String Username, String email,
                   String password , String NomorHP){
        this.UID = UID;
        this.Username = Username;
        this.Email = email;
        this.Password = password;
        this.NomorHP = NomorHP;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        Username = Username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNomorHP() {
        return NomorHP;
    }

    public void setNomorHP(String nomorHP) {
        NomorHP = nomorHP;
    }
    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

}
