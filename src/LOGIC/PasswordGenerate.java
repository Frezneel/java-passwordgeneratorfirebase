package LOGIC;

import java.util.Random;

public class PasswordGenerate {
    private final  String HURUF_KAPITAL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final  String HURUF_KECIL = "abcdefghijklmnopqrstuvwxyz";
    private final  String ANGKA = "0123456789";
    private final  String SIMBOL = "!@#$%^&*()-_+={}[];:,.<>/?";

    // Random Class
    private final Random random;


    public PasswordGenerate() {
        random = new Random();
    }

    public String generatePassword(int panjang, boolean onUppercase, boolean onLowercase, boolean onNumbers
            , boolean onSymbols){
        StringBuilder passwordBuilder = new StringBuilder();

        String validChar = "";
        if (onUppercase) validChar += HURUF_KAPITAL;
        if (onLowercase) validChar += HURUF_KECIL;
        if (onNumbers) validChar += ANGKA;
        if (onSymbols) validChar += SIMBOL;

        for (int i = 0; i < panjang; i++){
            int randomIndex = random.nextInt(validChar.length());

            char randomChar = validChar.charAt(randomIndex);

            passwordBuilder.append(randomChar);
        }
        return passwordBuilder.toString();
    }
}
