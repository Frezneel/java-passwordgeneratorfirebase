import FirebaseDB.Call;
import GUI.BerandaGUI;
import GUI.LoginFormGUI;
import GUI.PasswordGUI;
import GUI.RegisterFormGUI;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.swing.*;

import java.io.IOException;


public class AppLauncher {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Call.initializeFirebaseApp();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //new BerandaGUI().setVisible(true);
                new LoginFormGUI().setVisible(true);
            }
        });
    }
}
