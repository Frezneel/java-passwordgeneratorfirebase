package FirebaseDB;

import Data.AkunUID;
import GUI.LoginFormGUI;
import com.google.firebase.database.*;

import java.util.Locale;

public class MyFbaseDB {
    private static DatabaseReference databaseReference;
    private static String fbUser;
    private static String UID;
    private static Object lock = new Object();

    public static boolean register(String UID, String username, String password){
        try {
            if(!checkUser(username)){
                databaseReference = FirebaseDatabase.getInstance().getReference();
                databaseReference.child("Aplikasi").child("PasswordGenerator").child("Akun").child(UID).setValue(new AkunUID(UID,username,password), null);
                System.out.println("Lanjut : " + UID);
                return true;
            };
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    public static boolean checkUser (String username){
        try {
            databaseReference = FirebaseDatabase.getInstance().getReference();
            Query a = databaseReference.child("Aplikasi").child("PasswordGenerator").child("Akun").orderByChild("username").equalTo(username);
            a.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        fbUser = snapshot.child("username").getValue(String.class);
                        UID = snapshot.child("uid").getValue(String.class);
                        break;
                    }
                    synchronized (lock){
                        lock.notify();
                    }
                }
                @Override
                public void onCancelled(DatabaseError error) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        synchronized (lock){
            try {
                lock.wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        if (!username.equals(fbUser)) return false;

        return true;
    }

    public static boolean validateLogin(String username, String password) {
        if (checkUser(username)){
            if (checkPassword(password)){
                return true;
            }else{
                new LoginFormGUI().PesanError("Password yang dimasukkan salah");
            }
        }else{
            new LoginFormGUI().PesanError("Username Tidak Terdaftar");
        }
        return false;
    }

    private static String oPassword;
    private static boolean checkPassword(String password){
        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query konfirmPass = databaseReference.child("Aplikasi").child("PasswordGenerator").child("Akun").child(UID);
        konfirmPass.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                oPassword = snapshot.child("password").getValue().toString();
                synchronized (lock){
                    lock.notify();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
        synchronized (lock){
            try {
                lock.wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        if (password.equals(oPassword))return true;

        return false;
    }
}
